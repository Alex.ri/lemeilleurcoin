import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { retry, catchError } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class ApiService {

  endpoint: string = "https://offers.naminilamy.fr/";

  httpOptions = {
    headers : new HttpHeaders({
    'Access-Control-Allow-Origin': '*',
    'Content-Type' : 'application/json',
    'X-Requested-With': 'XMLHttpRequest'
    })
  };

  constructor(
    public http: HttpClient
  ) { }

  errorHandl(error) {
    let errorMessage = '';
    if(error.error instanceof ErrorEvent) {
      errorMessage = error.error.message;
    } else {
      errorMessage = `Error Code: ${error.status}\nMessage : ${error.message}`; 
    }
    console.log(errorMessage);
    return throwError(errorMessage);
  }


  addOffer(params: any) {
    return this.http.post(this.endpoint, params)
    .pipe(
      retry(1),
      catchError(this.errorHandl)
    )
  }

  getOffers() {
    return this.http.get(this.endpoint, this.httpOptions)
    .pipe(
      retry(1),
      catchError(this.errorHandl)
    )
  }

  getOffersById(id:string) {
    return this.http.get(this.endpoint+id, this.httpOptions)
    .pipe(
      retry(1),
      catchError(this.errorHandl)
    )
  }

}
