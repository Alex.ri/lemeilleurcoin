import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-add-offer',
  templateUrl: './add-offer.page.html',
  styleUrls: ['./add-offer.page.scss'],
})
export class AddOfferPage implements OnInit {
  name: string;
  name2: string;
  price: number;
  location: string;
  description: string;


  constructor() { }

  ngOnInit() {
  }

}
