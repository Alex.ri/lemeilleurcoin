import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { ApiService } from 'src/app/services/api/api.service';

@Component({
  selector: 'app-details-offer',
  templateUrl: './details-offer.page.html',
  styleUrls: ['./details-offer.page.scss'],
})
export class DetailsOfferPage implements OnInit {
  slideOpts = {
    initialSlide: 1,
    speed: 400
  };
  offerId: string;
  datas: any;

  constructor(
    private activatedRoute: ActivatedRoute,
    private api: ApiService
  ) { }

  ngOnInit() {
    this.offerId = this.activatedRoute.snapshot.paramMap.get('id');
    this.getOffer();
  }

  getOffer() {
    this.api.getOffersById(this.offerId).subscribe(resp => {
      console.log(resp);
      this.datas = resp;
    });
  }

}
