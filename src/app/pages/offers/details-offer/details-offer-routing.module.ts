import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { DetailsOfferPage } from './details-offer.page';

const routes: Routes = [
  {
    path: '',
    component: DetailsOfferPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class DetailsOfferPageRoutingModule {}
