import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { DetailsOfferPageRoutingModule } from './details-offer-routing.module';

import { DetailsOfferPage } from './details-offer.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    DetailsOfferPageRoutingModule
  ],
  declarations: [DetailsOfferPage]
})
export class DetailsOfferPageModule {}
