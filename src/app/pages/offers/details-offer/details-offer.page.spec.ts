import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { DetailsOfferPage } from './details-offer.page';

describe('DetailsOfferPage', () => {
  let component: DetailsOfferPage;
  let fixture: ComponentFixture<DetailsOfferPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DetailsOfferPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(DetailsOfferPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
