import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { ListOfferPage } from './list-offer.page';

describe('ListOfferPage', () => {
  let component: ListOfferPage;
  let fixture: ComponentFixture<ListOfferPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ListOfferPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(ListOfferPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
