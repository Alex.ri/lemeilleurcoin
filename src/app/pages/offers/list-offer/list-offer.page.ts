import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { NavController } from '@ionic/angular';
import { ApiService } from 'src/app/services/api/api.service';

@Component({
  selector: 'app-list-offer',
  templateUrl: './list-offer.page.html',
  styleUrls: ['./list-offer.page.scss'],
})
export class ListOfferPage implements OnInit {
  slideConfig = {
    spaceBetween: 10,
    centeredSlides: true,
    slidesPerView: 1.6,
    initialSlide: 1,
  }
  offers: any;
  
  constructor(
    private navCtrl:NavController,
    private router: Router,
    private api: ApiService
  ) { }

  ngOnInit() {
    this.getDatas();
  }

  getDatas(): void {
    this.api.getOffers().subscribe(resp => {
      console.log(resp)
      this.offers = resp;
    });
  }

  goDetails(id): void {
    console.log(id);
    this.router.navigate(['details-offer', id]);
    //this.navCtrl.navigateForward('/details-offer/' + id);
  }

}
