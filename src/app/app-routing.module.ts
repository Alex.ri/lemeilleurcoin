import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  {
    path: '',
    redirectTo: 'home',
    pathMatch: 'full'
  },
  {
    path: 'home',
    loadChildren: () => import('./pages/home/home.module').then( m => m.HomePageModule)
  },
  {
    path: 'details-offer/:id',
    loadChildren: () => import('./pages/offers/details-offer/details-offer.module').then( m => m.DetailsOfferPageModule)
  },
  {
    path: 'list-offer',
    loadChildren: () => import('./pages/offers/list-offer/list-offer.module').then( m => m.ListOfferPageModule)
  },
  {
    path: 'add-offer',
    loadChildren: () => import('./pages/offers/add-offer/add-offer.module').then( m => m.AddOfferPageModule)
  }
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules })
  ],
  exports: [RouterModule]
})
export class AppRoutingModule {}
